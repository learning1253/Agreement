package com.example.amenitiesManagement.repository;

import com.example.amenitiesManagement.model.entity.AmenitiesEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AmenitiesRepository extends MongoRepository<AmenitiesEntity,String> {

}
