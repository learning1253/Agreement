package com.example.amenitiesManagement.service;

import com.example.amenitiesManagement.enums.AmenitiesTypeEnum;
import com.example.amenitiesManagement.enums.GenderEnum;
import com.example.amenitiesManagement.enums.TimeEnum;
import com.example.amenitiesManagement.exceptions.AgeReservationException;
import com.example.amenitiesManagement.exceptions.NameLengthException;
import com.example.amenitiesManagement.exceptions.ReservationFullException;
import com.example.amenitiesManagement.exceptions.WrongSelectionException;
import com.example.amenitiesManagement.model.entity.AmenitiesEntity;
import com.example.amenitiesManagement.model.request.AmenitiesRequest;
import com.example.amenitiesManagement.model.response.AmenitiesResponse;
import com.example.amenitiesManagement.repository.AmenitiesRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static com.example.amenitiesManagement.constant.AgeRange.*;
import static com.example.amenitiesManagement.constant.AmenitiesTime.SCHEDULE_ONE;
import static com.example.amenitiesManagement.constant.AmenitiesTime.SCHEDULE_TWO;

@Service
@Slf4j
public class AmenitiesService {
    @Autowired
    AmenitiesRepository amenitiesRepository;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    MongoTemplate mongoTemplate;

    //ViewFees
    public String viewFees(AmenitiesTypeEnum userAmenityTypes, Date date, TimeEnum amenityTime) {
        String analysisResult = "";
        List<AmenitiesEntity> amenityEntities = getReservationQuery(userAmenityTypes.getAmenityTypes(),date,amenityTime.getAmenityTime());
        int reservationSize = amenityEntities.size();
        String[] reservationDetail = getReservationDetail(userAmenityTypes.getAmenityTypes(),amenityTime.getAmenityTime());
        if(reservationSize<Integer.parseInt(reservationDetail[0])){
            analysisResult = "Reservation is Available for Selected Amenity "+"\n"+"Fee for "+userAmenityTypes.getAmenityTypes()+" is "+ reservationDetail[1];
        }
        else{
            analysisResult = "Reservation Full for "+userAmenityTypes.getAmenityTypes();
        }
        return analysisResult;
    }

//Post
    public String createAmenityReservation( String name, AmenitiesTypeEnum userAmenityTypes, Date date, TimeEnum amenityTime, GenderEnum gender, Integer age) {
        AmenitiesRequest amenitiesRequest = new AmenitiesRequest();
        amenitiesRequest.setName(name);
        amenitiesRequest.setUserAmenityTypes(userAmenityTypes);
        amenitiesRequest.setDate(date);
        amenitiesRequest.setAmenityTime(amenityTime);
        amenitiesRequest.setUserGender(gender);
        amenitiesRequest.setAge(age);
        AmenitiesResponse amenitiesResponse = new AmenitiesResponse();
        String checkAmenity = amenitiesRequest.getUserAmenityTypes().getAmenityTypes();
        Date checkDate = amenitiesRequest.getDate();
        String checkTime = amenitiesRequest.getAmenityTime().getAmenityTime();
        String[] reservationCheckResult = getReservationDetail(checkAmenity,checkTime);
        int reservationLimit = Integer.parseInt(reservationCheckResult[0]);
        String reservationFee=reservationCheckResult[1];

        if(!nameCheck(amenitiesRequest.getName())){
            throw new NameLengthException();
        }

        List<AmenitiesEntity> amenitiesEntities =  getReservationQuery(checkAmenity, checkDate, checkTime);
        if (amenitiesEntities.size() < reservationLimit && ageCheck(amenitiesRequest.getUserAmenityTypes(),amenitiesRequest.getAge())) {
            AmenitiesEntity newAmenitiesEntity = modelMapper.map(amenitiesRequest, AmenitiesEntity.class);
            newAmenitiesEntity.setCreatedPerson(amenitiesRequest.getName());
            newAmenitiesEntity.setCreatedDateTime(LocalDateTime.now());
            amenitiesRepository.save(newAmenitiesEntity);
            amenitiesResponse = modelMapper.map(newAmenitiesEntity, AmenitiesResponse.class);
        } else {
            throw new ReservationFullException();
        }
        return "Reservation created for "+amenitiesResponse.getUserAmenityTypes() + "\n" +"User ID: "+amenitiesResponse.getId()+"\n"+ "Fee for " + checkAmenity + "=" + reservationFee;
    }


    //Put
    public String updateAmenityReservation(String id, String name, AmenitiesTypeEnum userAmenityTypes, Date date, TimeEnum amenityTime, GenderEnum userGender, Integer age) {
        AmenitiesResponse amenitiesResponse = new AmenitiesResponse();
        AmenitiesEntity amenitiesEntity = amenitiesRepository.findById(id).orElseThrow(() -> new RuntimeException("ID NOT FOUND ERROR"));
        String[] reservationCheckResult = new String[2];
        String updateUserAmenity = " ";
        if (amenitiesEntity != null) {
            String updateUserName = (StringUtils.isNotBlank(name) ? name : amenitiesEntity.getName());
            amenitiesEntity.setName(updateUserName);
            updateUserAmenity = (StringUtils.isNotBlank(String.valueOf(userAmenityTypes))) ? (userAmenityTypes.getAmenityTypes()) : (amenitiesEntity.getUserAmenityTypes());
            amenitiesEntity.setUserAmenityTypes(updateUserAmenity);
            Date updateUserDate = (StringUtils.isNotBlank(String.valueOf(date))) ? date : (Date) amenitiesEntity.getDate();
            amenitiesEntity.setDate(updateUserDate);
            String updateUserTime = (StringUtils.isNotBlank(String.valueOf(amenityTime))) ? (amenityTime.getAmenityTime()) : (amenitiesEntity.getAmenityTime());
            amenitiesEntity.setAmenityTime(updateUserTime);
            String updateUserGender = (StringUtils.isNotBlank(String.valueOf(userGender)) ? (userGender.getUserGender()) : amenitiesEntity.getUserGender());
            amenitiesEntity.setUserGender(updateUserGender);
            Integer updateUserAge = (StringUtils.isNotBlank(String.valueOf(age))) ? (age) : (amenitiesEntity.getAge());
            amenitiesEntity.setAge(updateUserAge);
            reservationCheckResult = getReservationDetail(updateUserAmenity, updateUserTime);

            if (!nameCheck(updateUserName)) {
                throw new NameLengthException();
            }

            List<AmenitiesEntity> amenityEntities = getReservationQuery(userAmenityTypes.getAmenityTypes(), date, amenityTime.getAmenityTime());
            if ((Objects.equals(userAmenityTypes.getAmenityTypes(), updateUserAmenity)) || amenityEntities.size() < Integer.parseInt(reservationCheckResult[0]) && ageCheck(userAmenityTypes, age)) {
                AmenitiesEntity newAmenityEntity = modelMapper.map(amenitiesEntity, AmenitiesEntity.class);
                newAmenityEntity.setLastUpdatedPerson(updateUserName);
                newAmenityEntity.setLastUpdatedDateTime(LocalDateTime.now());

                amenitiesRepository.save(newAmenityEntity);
                amenitiesResponse = modelMapper.map(newAmenityEntity, AmenitiesResponse.class);
            } else {
                throw new ReservationFullException();
            }
        }

        return "Reservation updated for "+amenitiesResponse.getUserAmenityTypes() + "\n" + "Fee for " + userAmenityTypes + " = " + reservationCheckResult[1];
    }

    //Get
    public AmenitiesResponse getAmenityReservation(String id) {
        AmenitiesEntity amenitiesEntity = amenitiesRepository.findById(id).orElseThrow(() -> new RuntimeException("ID not found"));
        return modelMapper.map(amenitiesEntity, AmenitiesResponse.class);
    }

    //Delete
    public String deleteAmenityReservation(String id) {
        amenitiesRepository.deleteById(id);
        return "Deleted";
    }

//-------------------Functions---------------------------------

    //Query
    private List getReservationQuery(String checkAmenity, Date checkDate, String checkTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userAmenityTypes").is(checkAmenity));
        query.addCriteria(Criteria.where("date").is(checkDate));
        query.addCriteria(Criteria.where("amenityTime").is(checkTime));
        List<AmenitiesEntity> users = mongoTemplate.find(query, AmenitiesEntity.class);
        return users;
    }

    //Reservation
    public String[] getReservationDetail(String amenity, String time) {
        String[] reservationDetail = new String[2];
        switch (amenity) {
            case "GYM":
                reservationDetail[0] = "10";
                if (SCHEDULE_ONE.contains(time)) {
                    reservationDetail[1] = "30 AED";
                } else if (SCHEDULE_TWO.contains(time)) {
                    reservationDetail[1] = "50 AED";
                }
                break;
            case "SWIMMING POOL":
                reservationDetail[0] = "5";
                if (SCHEDULE_ONE.contains(time)) {
                    reservationDetail[1] = "20 AED";
                } else if (SCHEDULE_TWO.contains(time)) {
                    reservationDetail[1] = "10 AED";
                }
                break;
            case "STEAM BATH":
                reservationDetail[0] = "2";
                if (SCHEDULE_ONE.contains(time)) {
                    reservationDetail[1] = "25 AED";
                } else if (SCHEDULE_TWO.contains(time)) {
                    reservationDetail[1] = "50 AED";
                }
                break;
            case "YOGA":
                reservationDetail[0] = "3";
                reservationDetail[1] = "50 AED";
                break;
            default:
                throw new WrongSelectionException();
        }
        return reservationDetail;
    }


    //User age checking
    public boolean ageCheck(AmenitiesTypeEnum userAmenityTypes, Integer age){
        boolean criteriaResult = false;
        if(userAmenityTypes.equals(AmenitiesTypeEnum.GYM)){
            if(age>=MIN_GYM_AGE && age<=MAX_GYM_AGE){
                criteriaResult = true;
            }
            else {
                throw new AgeReservationException();
            }
        }
        else if(userAmenityTypes.equals(AmenitiesTypeEnum.SWIMMING_POOL)){
            if(age>=MIN_SWIMMING_POOL_AGE && age<=MAX_SWIMMING_POOL_AGE){
                criteriaResult = true;
            }
            else {
                throw new AgeReservationException();
            }
        }
        else if(userAmenityTypes.equals(AmenitiesTypeEnum.STEAM_BATH)){
            if(age>=MIN_STEAM_BATH_AGE && age<=MAX_STEAM_BATH_AGE){
                criteriaResult = true;
            }
            else {
                throw new AgeReservationException();
            }
        }
        else if(userAmenityTypes.equals(AmenitiesTypeEnum.YOGA)){
            if(age>=MIN_YOGA_AGE && age<=MAX_YOGA_AGE){
                criteriaResult = true;
            }
            else {
                throw new AgeReservationException();
            }
        }

        return criteriaResult;
    }

    //User Name Length Checking
    public boolean nameCheck(String name){
        boolean criteriaResult = false;
        if(name.length()>=3 && name.length()<=20){
            criteriaResult =true;
        }
        return criteriaResult;
    }

}


