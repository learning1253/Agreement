package com.example.amenitiesManagement.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AmenitiesResponse {
    private String id;
    private String name;
    private String userAmenityTypes;
    private Date date;
    private String amenityTime;
    private String userGender;
    private Integer age;
}
