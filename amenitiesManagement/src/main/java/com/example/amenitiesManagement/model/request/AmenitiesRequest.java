package com.example.amenitiesManagement.model.request;

import com.example.amenitiesManagement.enums.AmenitiesTypeEnum;
import com.example.amenitiesManagement.enums.GenderEnum;
import com.example.amenitiesManagement.enums.TimeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AmenitiesRequest {
    @NotNull
    @Size(min = 3, max = 20, message = "Name must not exceed 20 characters")
    private String name;
    @NotNull
    private AmenitiesTypeEnum userAmenityTypes;
    @NotNull
    private Date date;
    @NotNull
    private TimeEnum amenityTime;
    @NotNull
    private GenderEnum userGender;
    @NotNull
    @Min(value=10, message = "Minimum age is 12")
    @Max(value=60, message="Maximum age is 60")
    private Integer age;
}
