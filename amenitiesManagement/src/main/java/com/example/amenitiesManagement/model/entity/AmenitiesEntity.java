package com.example.amenitiesManagement.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "amenitiesManagement")
public class AmenitiesEntity {
    @Id
    private String id;
    private String name;
    private String userAmenityTypes;
    private Date date;
    private String amenityTime;
    private String userGender;
    private Integer age;

    private LocalDateTime createdDateTime;
    private LocalDateTime lastUpdatedDateTime;
    private String createdPerson;
    private String lastUpdatedPerson;


}
