package com.example.amenitiesManagement.enums;

public enum AmenitiesTypeEnum {
    GYM("GYM"),
    SWIMMING_POOL("SWIMMING POOL"),
    STEAM_BATH("STEAM BATH"),
    YOGA("YOGA");
    private String amenityTypes;
    AmenitiesTypeEnum(final String amenity){
        amenityTypes = amenity;
    }
    public String getAmenityTypes(){
        return amenityTypes;
    }
}
