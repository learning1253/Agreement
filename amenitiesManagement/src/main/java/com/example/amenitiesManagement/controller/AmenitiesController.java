package com.example.amenitiesManagement.controller;

import com.example.amenitiesManagement.enums.AmenitiesTypeEnum;
import com.example.amenitiesManagement.enums.GenderEnum;
import com.example.amenitiesManagement.enums.TimeEnum;
import com.example.amenitiesManagement.exceptions.AgeReservationException;
import com.example.amenitiesManagement.exceptions.NameLengthException;
import com.example.amenitiesManagement.service.AmenitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.sql.Date;

@RestController
@RequestMapping("/reservation")
public class AmenitiesController {
    @Autowired
    AmenitiesService amenitiesService;

    @GetMapping("/viewFee")
    public ResponseEntity<?>viewFees(@RequestParam AmenitiesTypeEnum userAmenityTypes,@RequestParam Date date, @RequestParam TimeEnum amenityTime){
        return new ResponseEntity<>(amenitiesService.viewFees(userAmenityTypes,date,amenityTime),HttpStatus.OK);
    }

    @PostMapping("/addUserDetail")
    public ResponseEntity<?> setReservation(@RequestParam String name, @RequestParam AmenitiesTypeEnum userAmenityTypes, @RequestParam Date date, @RequestParam TimeEnum amenityTime, @RequestParam GenderEnum gender, @RequestParam Integer age){
        try {
            return new ResponseEntity(amenitiesService.createAmenityReservation(name, userAmenityTypes, date, amenityTime, gender, age), HttpStatus.OK);
        }
        catch (NameLengthException l){
            return new ResponseEntity<>(l.getMessage(),HttpStatus.BAD_REQUEST);
        }
        catch (AgeReservationException l){
            return new ResponseEntity<>(l.getMessage(),HttpStatus.BAD_REQUEST);
        }
        catch (Exception l){
            return new ResponseEntity<>(l.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/retrieve")
    public ResponseEntity<?> getReservation(@RequestParam String id){
        return new ResponseEntity(amenitiesService.getAmenityReservation(id),HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateReservation(@RequestParam String id , @RequestParam String name, @RequestParam AmenitiesTypeEnum userAmenityTypes, @RequestParam Date date, @RequestParam TimeEnum amenityTime, @RequestParam GenderEnum gender, @RequestParam Integer age){
        try {
            return new ResponseEntity(amenitiesService.updateAmenityReservation(id, name, userAmenityTypes, date, amenityTime, gender, age), HttpStatus.OK);
        }catch (NameLengthException l){
            return new ResponseEntity<>(l.getMessage(),HttpStatus.BAD_REQUEST);
        }catch (Exception l ){
            return new ResponseEntity<>(l.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> deleteReservation(@RequestParam String id){
        return new ResponseEntity(amenitiesService.deleteAmenityReservation(id),HttpStatus.OK);
    }
}
