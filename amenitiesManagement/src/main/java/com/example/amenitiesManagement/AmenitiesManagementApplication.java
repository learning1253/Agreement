package com.example.amenitiesManagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmenitiesManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmenitiesManagementApplication.class, args);
	}

}
