package com.example.amenitiesExperience.model.request;

import com.example.amenitiesExperience.enums.AmenitiesTypeEnum;
import com.example.amenitiesExperience.enums.GenderEnum;
import com.example.amenitiesExperience.enums.TimeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AmenitiesRequest {
    @NotNull
    @Size(min = 3, max = 20, message = "Name must not exceed 20 characters")
    private String name;
    @NotNull
    private AmenitiesTypeEnum userAmenityTypes;
    @NotNull
    private Date date;
    @NotNull
    private TimeEnum amenityTime;
    @NotNull
    private GenderEnum userGender;
    @NotNull
    @Min(value=10, message = "Minimum age is 12")
    @Max(value=60, message="Maximum age is 60")
    private Integer age;
}
