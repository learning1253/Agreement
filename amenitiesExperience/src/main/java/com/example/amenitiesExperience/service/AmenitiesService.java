package com.example.amenitiesExperience.service;

import com.example.amenitiesExperience.enums.AmenitiesTypeEnum;
import com.example.amenitiesExperience.enums.GenderEnum;
import com.example.amenitiesExperience.enums.TimeEnum;
import com.example.amenitiesExperience.model.request.AmenitiesRequest;
import com.example.amenitiesExperience.model.response.AmenitiesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class AmenitiesService {
    @Autowired
    AmenitiesInterface amenitiesInterface;
    public String createAmenityReservation(String name, AmenitiesTypeEnum userAmenityTypes, Date date, TimeEnum amenityTime, GenderEnum gender, Integer age) {
        return amenitiesInterface.newCreateAmenityReservation(name,userAmenityTypes,date,amenityTime,gender,age);
    }

    public AmenitiesResponse getAmenityReservation(String id) {
        return amenitiesInterface.newGetAmenityReservation(id);
    }

    public String updateAmenityReservation(String id, String name, AmenitiesTypeEnum userAmenityTypes, Date date, TimeEnum amenityTime, GenderEnum gender, Integer age) {
        return amenitiesInterface.newUpdateAmenityReservation(id,name,userAmenityTypes,date,amenityTime,gender,age);
    }

    public String deleteAmenityReservation(String id) {
        return amenitiesInterface.newDeleteAmenityReservation(id);
    }

    public String viewFees(AmenitiesTypeEnum userAmenityTypes, Date date, TimeEnum amenityTime) {
        return amenitiesInterface.newViewFees(userAmenityTypes,date,amenityTime);
    }
}
