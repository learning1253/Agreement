package com.example.amenitiesExperience.service;

import com.example.amenitiesExperience.enums.AmenitiesTypeEnum;
import com.example.amenitiesExperience.enums.GenderEnum;
import com.example.amenitiesExperience.enums.TimeEnum;
import com.example.amenitiesExperience.model.response.AmenitiesResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import java.sql.Date;

@FeignClient(name = "amenity-reservation", url = "${config.rest.service.addAmenitiesManagementUrl}")
public interface AmenitiesInterface {
    @PostMapping("/addUserDetail")
    public String newCreateAmenityReservation(@RequestParam String name, @RequestParam AmenitiesTypeEnum userAmenityTypes, @RequestParam Date date, @RequestParam TimeEnum amenityTime, @RequestParam GenderEnum gender, @RequestParam Integer age);

    @GetMapping("/retrieve")
    public AmenitiesResponse newGetAmenityReservation(@RequestParam String id);

    @PutMapping("/update")
    public String newUpdateAmenityReservation(@RequestParam String id , @RequestParam String name, @RequestParam AmenitiesTypeEnum userAmenityTypes, @RequestParam Date date, @RequestParam TimeEnum amenityTime, @RequestParam GenderEnum gender, @RequestParam Integer age);

    @DeleteMapping("/delete")
    public String newDeleteAmenityReservation(@RequestParam String id);

    @GetMapping("/viewFee")
    public String newViewFees(@RequestParam AmenitiesTypeEnum userAmenityTypes,@RequestParam Date date,@RequestParam TimeEnum amenityTime);
}
